

//  a spread operator example
var a = [1, 2, 3];
var b = [4, 5, 6];
var c = [...a, ...b];
// console.log(c);
// [1, 2, 3, 4, 5, 6]

// spread operator with object
var a = { name: "John" };
var b = { age: 30 };
var c = { ...a, ...b };
// console.log(c);
// { name: "John", age: 30 }


// rest operator example
function add(a,...args) {
    return args.map(el=>el+a)
}

var result = add(1,2,3,4,5);
console.log(result);
// [3, 4, 5, 6]













