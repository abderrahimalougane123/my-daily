import React, { Children, useEffect, useState } from 'react'


 export const CountryContext=React.createContext()



const CoutrriesContext = ({children}) => {



    const [countries, setCountries] = useState([]);

    useEffect(()=>{
    
      const GetAllCountries = async () => {
        const response = await fetch('https://restcountries.com/v3.1/all');
        const data = await response.json();
        console.log(data);
       setCountries(data);
      }
      return GetAllCountries;
    },[])




  return (
    <CountryContext.Provider value={{countries}}>
     {children}
    </CountryContext.Provider>
  )
}

export default CoutrriesContext
