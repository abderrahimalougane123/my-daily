// import './App.css';
import Countries from './components/Countries';
import { Row,PageContainer} from 'antd';
import 'antd/dist/antd.css'; 
// import { useEffect, useState ,useContext} from 'react';
import Navbar from './components/Navbar';
import { Routes,BrowserRouter as Router,Route,Navigate } from 'react-router-dom';
import CountryInfo from './components/CountryInfo';





function App() {




  return (


    <div className=' mt-4   mx-24 ' >
      <Router>
        <Navbar />
        <Routes>
          <Route path="*" element={<Navigate to="/Coutries"  />} />
          <Route path="/Coutries" element={<Countries  />} />
          <Route path="/Coutries/:name" element={<CountryInfo  />} />
        </Routes>
      </Router>


    </div>
    
    

  );
}

export default App;
