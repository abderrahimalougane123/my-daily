import React from 'react'
import { FaFlag } from 'react-icons/fa';
const Navbar = () => {
  return (
    <div className=' flex content-center px-3  py-2 rounded-md  bg-slate-700  mb-10 justify-between	'>

    <div >
    <FaFlag  className='text-white text-lg mt-1' />

        </div>
    <div>
    <a href="https://restcountries.com/#api-endpoints-v3-all" target="_blank" class="inline-flex items-center py-1 px-6 text-sm font-medium text-center  text-white bg-cyan-700  rounded-lg ">
           API
        </a>
    </div>
      
    </div>
  )
}

export default Navbar
