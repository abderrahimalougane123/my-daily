import React, { useContext } from 'react'
import { Card,Col ,Spin} from 'antd';
import Country from './Country';
import { useEffect, useState } from 'react';
import { CountryContext } from '../Context/CoutrriesContext';






const Countries = () => {

  const {countries}=useContext(CountryContext)


  const name:string="hello"
  console.log(countries)


  return  countries.length>0 ? (
<div  className=' grid   xl:grid-cols-4   lg:grid-cols-3  md:grid-cols-2  sm:grid-cols-1  gap-3'>
{countries.map(country=> {return (
      
      <Country   data={country}/>
      
      )})}
</div>
    
    
  ): <Spin  justify='center' />
}

export default Countries
