import { data } from 'autoprefixer';
import React, { useEffect,useState } from 'react'
import { Spin,Tag} from 'antd';


import { useParams } from 'react-router-dom';
const CountryInfo = () => {
const [country, setCountry] = useState({});
    const {name} = useParams();

    useEffect(()=>{

        const GetCountryInfo = async () => {
            const response = await fetch(`https://restcountries.com/v3.1/name/${name}`);
            const data = await response.json();
            console.log(data)

         
            console.log(name)
            setCountry(data); 
        }
        return GetCountryInfo;
    },[name])
    // console.log(name)
  return    country.length>0 ?(
    <>
        <div className='grid md:grid-cols-2 sm:grid-cols-2 gap-9'>
            <img src={country[0].flags.svg} alt={name} className='w-full'/>
            <div className='grid  md:grid-cols-2 sm:grid-cols-2 mt-6'>
                <div>
                <h1 className='text-2xl font-bold  '> {country[0].name.common}</h1>
                <p className='mt-10'> <span className='font-bold  text-lg'>Population:</span>{country[0].population} </p>
                <p  className='mt-10'><span className='font-bold text-lg'>Region:</span>{country[0].region}</p>
                <p  className='mt-10'><span className='font-bold text-lg'>Sub Region:</span>{country[0].subregion}</p>
                </div>
            <div>
            <p><span className='font-bold text-lg'>Top level Domain:</span>{country[0].tld[0]}</p>
            <p><span className='font-bold text-lg'>Currencies:</span>{Object.keys(country[0].currencies).map(  cu=> {return country[0].currencies[cu].name})}</p>
            <p><span className='font-bold text-lg' >Language:</span>{Object.keys(country[0].languages).map(  cu=> { return <Tag color="magenta">{country[0].languages[cu]}</Tag> })}</p>
            <p><span className='font-bold text-lg'>Capital:</span>{country[0].capital[0]}</p>
            </div>



            </div>
        </div>

        {/* <div className='grid md:grid-cols-1 mt-5 '>
        
        <iframe src={`<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7${country}1518.442574202!2d17.330033!3d26.360150000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f${}.1!3m3!1m2!1s0x13a892d98ece010d%3A0xfa076041c7f9c22a!2sLibye!5e0!3m2!1sfr!2sma!4v1657744260440!5m2!1sfr!2sma" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`} 
         allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" className='w-full h-60'></iframe>

            </div> */}
    
  </>
  ):<Spin  justify='center' />
}

export default CountryInfo
