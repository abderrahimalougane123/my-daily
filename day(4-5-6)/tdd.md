                                Day 4

1-Learn tdd(test driven development)

- tdd is a technique that allows you to write tests before you write code.
- tests are small pieces of code that are run to make sure that the code is working as intended.
- there are different types of tests: unit tests, integration tests, and end to end tests.
- unit tests are tests that test a single piece of code.
- integration tests are tests that test the whole application.
- end to end tests are tests that test the whole application from the client to the server using a semulator environment.
