        Day two

1-Learn web foundamentals

- http is a protocol that allows you to connect to a web server

- http requests are made to a server and the server responds with a response

- response is a message that is sent back to the client

- a server is a computer that runs a program

- a client is a computer that sends requests to a server

- a web browser is a client that sends requests to a server

- a web server is a server that responds to requests from a client
