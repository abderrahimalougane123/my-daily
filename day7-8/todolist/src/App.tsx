import React from 'react';
import logo from './logo.svg';
import './App.css';
import Task from './Components/AddTask';
import Tasks from './Components/Tasks';
import AddTask from './Components/AddTask';

function App() {
  return (
    <div className="App">
<AddTask/>
      <Tasks/>
    </div>
  );
}

export default App;
