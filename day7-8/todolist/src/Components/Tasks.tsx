import React, { useContext, useState } from 'react'
import { TaskCn } from '../Context/TaskContext'

import Task from './Task'
import {TaskProps} from './Task'




const Tasks = () => {


    const {Tasks}=useContext(TaskCn)
    // console.log(Tasks)
  return ( 
    <div>
      {Tasks.map((ts:TaskProps)=>{return <Task id={ts.id} key={ts.id} content={ts.content} deadline={ts.deadline} completed={ts.completed}/>})}
    </div>
  )
}

export default Tasks
