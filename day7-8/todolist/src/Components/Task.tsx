import { type } from '@testing-library/user-event/dist/type'
import React, { useContext } from 'react'
import { AiOutlineClose } from "react-icons/ai";
import { TaskCn } from '../Context/TaskContext';


export type TaskProps = {
        id?:number,
        content: string,
        deadline: number,
        completed: boolean

}


const Task = ({content,deadline,completed,id}:TaskProps) => {

    const {Tasks,setTasks}=useContext(TaskCn)
    const handelClick=()=>{

        const newTasks=Tasks.filter((ts:TaskProps)=>ts.id!==id)
        console.log(newTasks)
        setTasks(newTasks)



    }
    
  return (
    <div>
        <p>{content}</p>
        <p>{deadline}</p>
        <p>{completed}</p>
        <AiOutlineClose    style={{cursor:"pointer"}} onClick={()=>handelClick()}/>

        
    </div>
  )
}

export default Task
