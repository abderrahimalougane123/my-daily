import React, { useContext, useEffect, useState } from 'react'
import { TaskCn } from '../Context/TaskContext'
import { v4 as uuidv4 } from 'uuid';


const AddTask = () => {
  const {setTasks,Tasks} = useContext(TaskCn)
// console.log(Tasks[Tasks.length-1].id+1)
const [OneTask,setOneTask] =useState({content:'',deadline:0,completed:false})




  const handleChange = (e:any) => {
    // {content:'',deadline:0,completed:false}
    

   
    setOneTask({...OneTask,[e.target.name]:e.target.value})
  }
  const handleSubmit = (e:any) => {
    if(OneTask.content===""  || OneTask.deadline===0 ){

      alert('Please Fill  all fields')
      return
    }
    
    e.preventDefault()
    if(Tasks.length===0){

      setTasks([{...OneTask,id:1}])
      console.log(OneTask)

      return
    }
    setTasks((prev:any)=>[...prev,{...OneTask,id:Tasks[Tasks.length-1].id+1}])

  }
  return (
    <div>
        <input type="text"  value={OneTask.content} placeholder="Task Content"   name='content'  onChange={(e)=>handleChange(e)} />
        <input type="number" value={OneTask.deadline} placeholder="DeadLine" name='deadline'onChange={(e)=>handleChange(e)} />
        <button  type='submit'  onClick={(e)=>handleSubmit(e)}  >Add Task</button>
      
    </div> 
  )
}

export default AddTask
