import React, { useState } from 'react'


export const TaskCn=React.createContext()
const TaskContext = ({children}) => {

    
    const [Tasks,setTasks] =useState([
      
    ])
  return (
    <TaskCn.Provider value={{Tasks,setTasks}}>

        {children}
    </TaskCn.Provider>
   
  )
}

export default TaskContext
